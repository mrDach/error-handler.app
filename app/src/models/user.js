import mongoose from 'mongoose';
import bcrypt from 'bcrypt-nodejs';
import {AuthError} from "../helpers/APIError";

const UserSchema = new mongoose.Schema({
    email: { type: String, unique: true, required: true },
    fullName: { type: String, required: false },
    password: { type: String, required: true, select: false },
    hash: String,
    salt: String
});


/**
 * converts the string value of the password to some hashed value
 * - pre-save hooks
 * - validations
 * - virtuals
 */
UserSchema.pre('save', function userSchemaPre(next) {
    const user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, (err, salt) => {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, null, (hashErr, hash) => {
                if (hashErr) {
                    return next(hashErr);
                }
                user.password = hash;
                return next();
            });
        });
    } else {
        return next();
    }
});

UserSchema.pre("findOneAndUpdate", function(next) {
    const password = this.getUpdate().$set.password;
    if (!password) {
        return next();
    }
    try {
        bcrypt.genSalt(10, (err, salt) => {
            if (err) {
                return next(err);
            }
            bcrypt.hash(this.getUpdate().$set.password, salt, null, (hashErr, hash) => {
                if (hashErr) {
                    return next(hashErr);
                }
                this.getUpdate().$set.password = hash;
                return next();
            });
        });
    } catch (error) {
        return next(error);
    }
});

/**
 * comapare the stored hashed value of the password with the given value of the password
 * @param pw - password whose value has to be compare
 */
UserSchema.methods.comparePassword = async function comparePassword(pw) {
    const that = this;
    try {
        if (!await bcrypt.compareSync(pw, that.password)) {
            throw new AuthError('Incorrect login or password');
        }
    } catch (err) {
        throw new AuthError('Incorrect login or password');
    }
};

export default mongoose.model('User', UserSchema);