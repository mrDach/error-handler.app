import mongoose from 'mongoose';
import mongooseIntl from "mongoose-intl";

import config from "../config";

const BranchSchema = new mongoose.Schema({
    title: {type: String, required: true, intl: true},
    fullName: {type: String, required: true},
    email: {type: String, required: true},
    file: {
        filename: {type: String, required: true},
        name: {type: String, required: true},
        type: {type: String, required: false},
        size: {type: String, required: false},
    },
    address: {type: String, required: false},
    additionalInfo: {type: String, required: false},
    workTime: {
        open: {type: Number, required: true},
        close: {type: Number, required: true},
    },
    tel: {type: String, required: false},
    position: {
        lat: {type: Number, required: true},
        lng: {type: Number, required: true},
    },
    isPublic: {type: Boolean, default: true, required: true},
    createdAt: {type: Date, default: Date.now, required: true},
    updatedAt: {type: Date, default: Date.now, required: true},
});

const updateTimestemps = function (next) {
    const self = this;
    self.update({}, {$set: {updatedAt: Date.now()}});
    next();
};

BranchSchema
    .pre('update', updateTimestemps)
    .pre('findOneAndUpdate', updateTimestemps);

BranchSchema.plugin(mongooseIntl, config.mongooseIntl);
BranchSchema.set('toObject', {
    getters: false,
    virtuals: false,
    versionKey: false,
});
export default mongoose.model('Branch', BranchSchema);
