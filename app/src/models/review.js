import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const ReviewSchema = new mongoose.Schema({
    title: {type: String, required: true},
    text: {type: String, required: false},
    rating: {type: Number, required: true},
    authorName: {type: String, required: false},
    branchId: { type: Schema.Types.ObjectId, ref: 'Branch' },
    isPublic: {type: Boolean, default: true, required: true},
    priority: {type: Number, required: false},
    createdAt: {type: Date, default: Date.now, required: true},
    updatedAt: {type: Date, default: Date.now, required: true},
});

const updateTimestemps = function (next) {
    const self = this;
    self.update({}, {$set: {updatedAt: Date.now()}});
    next();
};

ReviewSchema
    .pre('save', updateTimestemps)
    .pre('update', updateTimestemps)
    .pre('findOneAndUpdate', updateTimestemps);

export default mongoose.model('Review', ReviewSchema);
