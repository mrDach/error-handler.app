/**
 * @apiDefine HasSuccessInResponse
 * @apiSuccess {Boolean} success    Request is finished successfully.
 */

/**
 * @apiDefine HasMongoId
 * @apiParam {String} [_id]   Unique id of record. If unset it's create new record.
 */

/**
 * @apiDefine HasMongoIdAsParam
 * @apiParam {String} _id   Unique id of record.
 */

/**
 * @apiDefine HasIsPublic
 * @apiParam {Boolean} [isPublic]     Show this record on frontend.
 */

/**
 * @apiDefine HasIsPublicAsPram
 * @apiParam {Boolean} isPublic     Show this record on frontend.
 */

/**
 * @apiDefine HasTimestamp
 * @apiParam {Date} [createdAt]   Date create of record. This field is filling automated.
 * @apiParam {Date} [updatedAt]   Date last update of record. This field is filling automated.
 */

/**
 * @apiDefine HasPriority
 * @apiParam {Number} [priority]  Position of record in sorted list. This field is filling automated.
 */

/**
 * @apiDefine HasSortedList
 *
 * @apiParam {Array} list           Sorted List.
 * @apiParam {String} list._id      Unique id of record.
 * @apiParam {Number} list.priority Position of record in sorted list.
 */

/**
 * @api {OBJECT} File File
 * @apiGroup _Custom types
 * @apiVersion 2.4.0
 *
 * @apiParam {String} name        Originally name of file.
 * @apiParam {String} filename    Name of file on disc.
 * @apiParam {String} type        MIME type of file.
 * @apiParam {String} size        Size in bites of file.
 */

/**
 * @api {OBJECT} LocalizeString LocalizeString
 * @apiGroup _Custom types
 * @apiVersion 2.4.0
 *
 * @apiParam {String} en    English version of string. This locale is default.
 * @apiParam {String} [ar]    Arabic version of string.
 * @apiParam {String} [zh]    Chinese version of string.
 * @apiParam {String} [hi]    Hindi version of string.
 * @apiParam {String} [ja]    Japanese version of string.
 * @apiParam {String} [ko]    Korean version of string.
 * @apiParam {String} [fr]    French version of string.
 * @apiParam {String} [de]    German version of string.
 * @apiParam {String} [it]    Italian version of string.
 * @apiParam {String} [pl]    Polish version of string.
 * @apiParam {String} [pt]    Portuguese version of string.
 * @apiParam {String} [es]    Spanish version of string.
 * @apiParam {String} [ru]    Russian version of string.
 */

/**
 * @apiDefine HasCurrentLanguage
 *
 * @apiParam {String="en","ar","zh","hi","ja","ko","fr","de","it","pl","pt","es","ru"} currentLanguage    Selected locale. It mast be <code>"en"</code> if it's create new record.
 */

/**
 * @apiDefine HasUnpublicPartner
 *
 * @apiParam {Boolean} isOwner
 * @apiParam {String{isURL}} [url]
 * @apiParam {String{..300}} headquarters
 * @apiParam {String{2..300}} fullName
 * @apiParam {String{isValidPhone}} phone
 * @apiParam {String{..255, isValidEmail}} email
 * @apiParam {String{..255, isValidSkypeUsername}} [skype]
 * @apiParam {String{isValidTelegramUsername}} [telegram]
 * @apiParam {[File](#api-_Custom_types-ObjectFile){maxSize(3M), acceptTypes(image/jpeg, image/jpg, image/png, image/svg+xml)}} logo
 * @apiParam {[File](#api-_Custom_types-ObjectFile){maxSize(3M), acceptTypes(image/jpeg, image/jpg, image/png)}} photo
 */

/**
 * @apiDefine HasPartner
 *
 * @apiParam {Boolean} isOwner
 * @apiParam {String{isURL}} [url]
 * @apiParam {String{..300}} headquarters
 * @apiParam {String{2..300}} [fullName]
 * @apiParam {String{isValidPhone}} [phone]
 * @apiParam {String{..255, isValidEmail}} [email]
 * @apiParam {String{..255, isValidSkypeUsername}} [skype]
 * @apiParam {String{isValidTelegramUsername}} [telegram]
 * @apiParam {[LocalizeString](#api-_Custom_types-ObjectLocalizestring){..500}} description
 * @apiParam {[File](#api-_Custom_types-ObjectFile){maxSize(3M), acceptTypes(image/jpeg, image/jpg, image/png, image/svg+xml)}} logo
 * @apiParam {[File](#api-_Custom_types-ObjectFile){maxSize(3M), acceptTypes(image/jpeg, image/jpg, image/png)}} photo
 */

/**
 * @api {OBJECT} Partner Partner
 * @apiGroup _Custom types
 * @apiVersion 2.4.0
 *
 * @apiUse HasMongoId
 * @apiUse HasIsPublic
 * @apiUse HasPriority
 * @apiUse HasTimestamp
 * @apiUse HasPartner
 *
 * @apiParam {Object} industryId
 * @apiParam {String} industryId._id
 * @apiParam {[LocalizeString](#api-_Custom_types-ObjectLocalizestring)} industryId.title
 */

/**
 * @apiDefine HasUnpublicBranch
 *
 * @apiParam {[File](#api-_Custom_types-ObjectFile){maxSize(3M), acceptTypes(image/jpeg, image/jpg, image/png)}} file
 * @apiParam {Object} workTime
 * @apiParam {Number{0..(workTime.close-60)}} workTime.open         Seconds from start of day.
 * @apiParam {Number{(workTime.open+60)..86400}} workTime.close     Seconds from start of day.
 * @apiParam {Object} position
 * @apiParam {String} position.lat
 * @apiParam {String} position.lng
 * @apiParam {[LocalizeString](#api-_Custom_types-ObjectLocalizestring)} title
 * @apiParam {String{2..300}} fullName
 * @apiParam {String{..255, isValidEmail}} email
 * @apiParam {String} address
 * @apiParam {String{..300}} [additionalInfo]
 * @apiParam {String{isValidPhone}} tel
 */

/**
 * @apiDefine HasBranch
 *
 * @apiParam {[File](#api-_Custom_types-ObjectFile){maxSize(3M), acceptTypes(image/jpeg, image/jpg, image/png)}} file
 * @apiParam {Object} workTime
 * @apiParam {Number{0..(workTime.close-60)}} workTime.open         Seconds from start of day.
 * @apiParam {Number{(workTime.open+60)..86400}} workTime.close     Seconds from start of day.
 * @apiParam {Object} position
 * @apiParam {String} position.lat
 * @apiParam {String} position.lng
 * @apiParam {[LocalizeString](#api-_Custom_types-ObjectLocalizestring)} title
 * @apiParam {String{2..300}} fullName
 * @apiParam {String{..255, isValidEmail}} email
 * @apiParam {String} address
 * @apiParam {String{..300}} [additionalInfo]
 * @apiParam {String{isValidPhone}} tel
 */

/**
 * @api {OBJECT} Branch Branch
 * @apiGroup _Custom types
 * @apiVersion 2.4.0
 *
 * @apiUse HasMongoId
 * @apiUse HasIsPublic
 * @apiUse HasPriority
 * @apiUse HasTimestamp
 * @apiUse HasBranch
 */

/**
 * @apiDefine HasReview
 *
 * @apiParam {Number} rating
 * @apiParam {String} title
 * @apiParam {String} authorName
 * @apiParam {String} text
 * @apiParam {String} branchId
 */

/**
 * @api {OBJECT} Review Review
 * @apiGroup _Custom types
 * @apiVersion 2.4.0
 *
 * @apiUse HasMongoId
 * @apiUse HasIsPublic
 * @apiUse HasPriority
 * @apiUse HasTimestamp
 * @apiUse HasReview
 */
