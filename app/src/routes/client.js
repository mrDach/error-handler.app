import express from 'express';

import {getPublicBranch, getBranch, saveBranch} from "../controllers/branches";

import {checkValidationErrors} from "../middleware/checkValidationErrors";
import {uploadBranches} from "../middleware/upload";

import {getPublicBranchValidator, saveUnpublicBranchValidator} from "../validators/branches";

const clientApp = express.Router();

/**
 * @api {GET} /api/client/branches     Get list of only public Branches.
 * @apiName getPublicBranch
 * @apiGroup Branches
 * @apiVersion 2.4.0
 *
 * @apiUse HasSuccessInResponse
 * @apiParam {String{2..300, isValidSearch}} [address]
 *
 * @apiSuccess {[Branch](#api-_Custom_types-ObjectBranch)[]} branches       List of Branches.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *           "success": true,
 *           "branches": [{
 *               "file": {
 *                   "name": "EzyMart.jpg",
 *                   "filename": "962160a7-2a87-4bdf-9011-20bda11dcf2c.jpg",
 *                   "type": "image/jpeg",
 *                   "size": "126616"
 *               },
 *               "workTime": {
 *                   "open": "28800",
 *                   "close": "46800"
 *               },
 *               "position": {
 *                   "lat": -33.8738897,
 *                   "lng": 151.22500130000003
 *               },
 *               "title": {
 *                   "en": "Ezymart"
 *               },
 *               "isPublic": true,
 *               "_id": "5c87866dbb80bb003906d5a6",
 *               "fullName": "Admin",
 *               "email": "admin@default.default",
 *               "address": "6 Roslyn St, Potts Point NSW 2011, Australia",
 *               "tel": "61 29 358 2803",
 *               "createdAt": "2019-03-12T10:14:05.267Z",
 *               "updatedAt": "2019-03-12T10:14:05.267Z",
 *               "__v": 0
 *           }]
 *     }
 */
clientApp.get('/branches', [getPublicBranchValidator, checkValidationErrors], getPublicBranch);

/**
 * @api {GET} /api/client/branches/:id     get Branch information by id.
 * @apiName getBranch
 * @apiGroup Branches
 * @apiVersion 2.4.0
 *
 * @apiUse HasSuccessInResponse
 * @apiUse HasMongoIdAsParam
 *
 * @apiSuccess {[Branch](#api-_Custom_types-ObjectBranch)} branch       Branch information or Null.
 *
 * @apiSuccessExample Success-Response: Branch is found.
 *     HTTP/1.1 200 OK
 *     {
 *           "success": true,
 *           "branch": {
 *               "file": {
 *                   "name": "EzyMart.jpg",
 *                   "filename": "962160a7-2a87-4bdf-9011-20bda11dcf2c.jpg",
 *                   "type": "image/jpeg",
 *                   "size": "126616"
 *               },
 *               "workTime": {
 *                   "open": "28800",
 *                   "close": "46800"
 *               },
 *               "position": {
 *                   "lat": -33.8738897,
 *                   "lng": 151.22500130000003
 *               },
 *               "title": {
 *                   "en": "Ezymart"
 *               },
 *               "isPublic": true,
 *               "_id": "5c87866dbb80bb003906d5a6",
 *               "fullName": "Admin",
 *               "email": "admin@default.default",
 *               "address": "6 Roslyn St, Potts Point NSW 2011, Australia",
 *               "tel": "61 29 358 2803",
 *               "createdAt": "2019-03-12T10:14:05.267Z",
 *               "updatedAt": "2019-03-12T10:14:05.267Z",
 *               "__v": 0
 *           }
 *     }
 *
 * @apiSuccessExample Success-Response: Branch is not found.
 *     HTTP/1.1 200 OK
 *     {
 *           "success": true,
 *           "branch": null
 *     }
 */
clientApp.get('/branch/:id', getBranch);

/**
 * @api {POST} /api/client/branch/save     Edit or create Unpublic Branch.
 * @apiName saveUnpublicBranch
 * @apiGroup Branches
 * @apiVersion 2.4.0
 *
 * @apiUse HasSuccessInResponse
 * @apiUse HasUnpublicBranch
 * @apiUse HasCurrentLanguage
 *
 * @apiSuccess {[Branch](#api-_Custom_types-ObjectBranch)} branch       Branch information.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *           "success": true,
 *           "branch": {
 *               "file": {
 *                   "name": "EzyMart.jpg",
 *                   "filename": "962160a7-2a87-4bdf-9011-20bda11dcf2c.jpg",
 *                   "type": "image/jpeg",
 *                   "size": "126616"
 *               },
 *               "workTime": {
 *                   "open": "28800",
 *                   "close": "46800"
 *               },
 *               "position": {
 *                   "lat": -33.8738897,
 *                   "lng": 151.22500130000003
 *               },
 *               "title": {
 *                   "en": "Ezymart"
 *               },
 *               "isPublic": true,
 *               "_id": "5c87866dbb80bb003906d5a6",
 *               "fullName": "Admin",
 *               "email": "admin@default.default",
 *               "address": "6 Roslyn St, Potts Point NSW 2011, Australia",
 *               "tel": "61 29 358 2803",
 *               "createdAt": "2019-03-12T10:14:05.267Z",
 *               "updatedAt": "2019-03-12T10:14:05.267Z",
 *               "__v": 0
 *           }
 *     }
 */
clientApp.post('/branch/save', [uploadBranches, saveUnpublicBranchValidator, checkValidationErrors], saveBranch);

export default clientApp;
