import express from 'express';
import path from 'path';
import config from "../config";

const indexApp = express.Router();

const sendIndex = (request, response) => {
    response.status(200);
    const filePath = path.resolve(__dirname, '../../../clientApp/client/build', 'index.html');
    response.sendFile(filePath);
};

indexApp.get('/', sendIndex);
indexApp.get('/platform', sendIndex);
indexApp.get('/wallets', sendIndex);
indexApp.get('/apollo-coin', sendIndex);
indexApp.get('/documents', sendIndex);
indexApp.get('/team', sendIndex);
indexApp.get('/roadmap', sendIndex);
indexApp.get('/faq', sendIndex);
indexApp.get('/stay-tuned', sendIndex);
indexApp.get('/store', sendIndex);
indexApp.get('/partners', sendIndex);
indexApp.get('/decentralized-bank', sendIndex);

config.mongooseIntl.languages.forEach(value => {
    indexApp.get(`/${value}`, sendIndex);
    indexApp.get(`/${value}/platform`, sendIndex);
    indexApp.get(`/${value}/wallets`, sendIndex);
    indexApp.get(`/${value}/apollo-coin`, sendIndex);
    indexApp.get(`/${value}/documents`, sendIndex);
    indexApp.get(`/${value}/team`, sendIndex);
    indexApp.get(`/${value}/roadmap`, sendIndex);
    indexApp.get(`/${value}/faq`, sendIndex);
    indexApp.get(`/${value}/stay-tuned`, sendIndex);
    indexApp.get(`/${value}/store`, sendIndex);
    indexApp.get(`/${value}/partners`, sendIndex);
    indexApp.get(`/${value}/decentralized-bank`, sendIndex);
});

export default indexApp;
