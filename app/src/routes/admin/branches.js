import express from 'express';

import {deleteBranch, getBranch, getBranches, saveBranch, togglePublicBranch} from "../../controllers/branches";
import {
    deleteBranchValidator,
    getBranchValidator,
    saveBranchValidator,
    togglePublicBranchValidator
} from "../../validators/branches";

import {validateAdmin} from "../../middleware/jwt-validation";
import {checkValidationErrors} from "../../middleware/checkValidationErrors";
import {uploadBranches} from "../../middleware/upload";

const branchesApi = express.Router();

/**
 * @api {GET} /api/admin/branches     Get list of all Branches
 * @apiName getBranches
 * @apiGroup Branches
 * @apiVersion 2.4.0
 *
 * @apiUse HasSuccessInResponse
 *
 * @apiSuccess {[Branch](#api-_Custom_types-ObjectBranch)[]} branches       List of Branches.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *           "success": true,
 *           "branches": [{
 *               "file": {
 *                   "name": "EzyMart.jpg",
 *                   "filename": "962160a7-2a87-4bdf-9011-20bda11dcf2c.jpg",
 *                   "type": "image/jpeg",
 *                   "size": "126616"
 *               },
 *               "workTime": {
 *                   "open": "28800",
 *                   "close": "46800"
 *               },
 *               "position": {
 *                   "lat": -33.8738897,
 *                   "lng": 151.22500130000003
 *               },
 *               "title": {
 *                   "en": "Ezymart"
 *               },
 *               "isPublic": true,
 *               "_id": "5c87866dbb80bb003906d5a6",
 *               "fullName": "Admin",
 *               "email": "admin@default.default",
 *               "address": "6 Roslyn St, Potts Point NSW 2011, Australia",
 *               "tel": "61 29 358 2803",
 *               "createdAt": "2019-03-12T10:14:05.267Z",
 *               "updatedAt": "2019-03-12T10:14:05.267Z",
 *               "__v": 0
 *           }]
 *     }
 */
branchesApi.get('/branches', validateAdmin, getBranches);

/**
 * @api {GET} /api/admin/branches/:id     get Branch information by id
 * @apiName getBranch
 * @apiGroup Branches
 * @apiVersion 2.4.0
 *
 * @apiUse HasSuccessInResponse
 * @apiUse HasMongoIdAsParam
 *
 * @apiSuccess {[Branch](#api-_Custom_types-ObjectBranch)} branch       Branch information or Null.
 *
 * @apiSuccessExample Success-Response: Branch is found.
 *     HTTP/1.1 200 OK
 *     {
 *           "success": true,
 *           "branch": {
 *               "file": {
 *                   "name": "EzyMart.jpg",
 *                   "filename": "962160a7-2a87-4bdf-9011-20bda11dcf2c.jpg",
 *                   "type": "image/jpeg",
 *                   "size": "126616"
 *               },
 *               "workTime": {
 *                   "open": "28800",
 *                   "close": "46800"
 *               },
 *               "position": {
 *                   "lat": -33.8738897,
 *                   "lng": 151.22500130000003
 *               },
 *               "title": {
 *                   "en": "Ezymart"
 *               },
 *               "isPublic": true,
 *               "_id": "5c87866dbb80bb003906d5a6",
 *               "fullName": "Admin",
 *               "email": "admin@default.default",
 *               "address": "6 Roslyn St, Potts Point NSW 2011, Australia",
 *               "tel": "61 29 358 2803",
 *               "createdAt": "2019-03-12T10:14:05.267Z",
 *               "updatedAt": "2019-03-12T10:14:05.267Z",
 *               "__v": 0
 *           }
 *     }
 *
 * @apiSuccessExample Success-Response: Branch is not found.
 *     HTTP/1.1 200 OK
 *     {
 *           "success": true,
 *           "branch": null
 *     }
 */
branchesApi.get('/branches/:id', [validateAdmin, getBranchValidator, checkValidationErrors], getBranch);

/**
 * @api {POST} /api/admin/branches/save     Edit or create Branch.
 * @apiName saveBranch
 * @apiGroup Branches
 * @apiVersion 2.4.0
 *
 * @apiUse HasSuccessInResponse
 * @apiUse HasMongoId
 * @apiUse HasIsPublic
 * @apiUse HasBranch
 * @apiUse HasCurrentLanguage
 *
 * @apiSuccess {[Branch](#api-_Custom_types-ObjectBranch)} branch       Branch information.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *           "success": true,
 *           "branch": {
 *               "file": {
 *                   "name": "EzyMart.jpg",
 *                   "filename": "962160a7-2a87-4bdf-9011-20bda11dcf2c.jpg",
 *                   "type": "image/jpeg",
 *                   "size": "126616"
 *               },
 *               "workTime": {
 *                   "open": "28800",
 *                   "close": "46800"
 *               },
 *               "position": {
 *                   "lat": -33.8738897,
 *                   "lng": 151.22500130000003
 *               },
 *               "title": {
 *                   "en": "Ezymart"
 *               },
 *               "isPublic": true,
 *               "_id": "5c87866dbb80bb003906d5a6",
 *               "fullName": "Admin",
 *               "email": "admin@default.default",
 *               "address": "6 Roslyn St, Potts Point NSW 2011, Australia",
 *               "tel": "61 29 358 2803",
 *               "createdAt": "2019-03-12T10:14:05.267Z",
 *               "updatedAt": "2019-03-12T10:14:05.267Z",
 *               "__v": 0
 *           }
 *     }
 */
branchesApi.post('/branches/save', [validateAdmin, uploadBranches, saveBranchValidator, checkValidationErrors], saveBranch);

/**
 * @api {POST} /api/admin/branches/public     Toggle isPublic flag of selected Branch.
 * @apiName togglePublicBranch
 * @apiGroup Branches
 * @apiVersion 2.4.0
 *
 * @apiUse HasSuccessInResponse
 * @apiUse HasMongoIdAsParam
 * @apiUse HasIsPublicAsPram
 *
 * @apiSuccess {[Branch](#api-_Custom_types-ObjectBranch)} branch       Branch information.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *           "success": true,
 *           "branch": {
 *               "file": {
 *                   "name": "EzyMart.jpg",
 *                   "filename": "962160a7-2a87-4bdf-9011-20bda11dcf2c.jpg",
 *                   "type": "image/jpeg",
 *                   "size": "126616"
 *               },
 *               "workTime": {
 *                   "open": "28800",
 *                   "close": "46800"
 *               },
 *               "position": {
 *                   "lat": -33.8738897,
 *                   "lng": 151.22500130000003
 *               },
 *               "title": {
 *                   "en": "Ezymart"
 *               },
 *               "isPublic": true,
 *               "_id": "5c87866dbb80bb003906d5a6",
 *               "fullName": "Admin",
 *               "email": "admin@default.default",
 *               "address": "6 Roslyn St, Potts Point NSW 2011, Australia",
 *               "tel": "61 29 358 2803",
 *               "createdAt": "2019-03-12T10:14:05.267Z",
 *               "updatedAt": "2019-03-12T10:14:05.267Z",
 *               "__v": 0
 *           }
 *     }
 */
branchesApi.post('/branches/public', [validateAdmin, togglePublicBranchValidator, checkValidationErrors], togglePublicBranch);

/**
 * @api {DELETE} /api/admin/branches/delete     Delete selected Branch.
 * @apiName deleteBranch
 * @apiGroup Branches
 * @apiVersion 2.4.0
 *
 * @apiUse HasSuccessInResponse
 * @apiUse HasMongoIdAsParam
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *           "success": true,
 *     }
 */
branchesApi.delete('/branches/delete', [validateAdmin, deleteBranchValidator, checkValidationErrors], deleteBranch);

export default branchesApi;
