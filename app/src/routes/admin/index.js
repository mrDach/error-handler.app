import express from 'express';

import branchesApi from "./branches";

const adminApi = express.Router();

adminApi.use(branchesApi);

export default adminApi;
