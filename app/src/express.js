import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import compress from 'compression';
import methodOverride from 'method-override';
import cors from 'cors';
import helmet from 'helmet';
import path from 'path';
import passport from 'passport';

import passConfig from './passport-config';

import adminApi from './routes/admin';
import client from './routes/client';
import sendIndex from './routes/sendIndex';
import {addExpressValidator} from "./middleware/expressValidator";
import {robots} from "./middleware/robots";
import {notFound} from "./middleware/notFound";
import {errorConverter} from "./middleware/errorConverter";
import {errorHandler} from "./middleware/errorHandler";

const app = express();
const server = require('http').createServer(app);

// enable CORS - Cross Origin Resource Sharing
app.use(cors());

// parse body params and attache them to req.body
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: true, limit: '50mb'}));

app.use(cookieParser());
app.use(compress());
app.use(methodOverride());

app.use((err, req, res, next) => {
    console.log(req.url);
    return next();
});

// configure passport for authentication
passConfig(passport);
app.use(passport.initialize());

// secure apps by setting various HTTP headers
app.use(helmet());

app.use(addExpressValidator);

// mount all routes on /api path
app.use('/api/admin', adminApi);
app.use('/api/client', client);

app.use(express.static(path.resolve(__dirname, '../../clientApp/client/build')));

app.use('/upload', express.static(path.resolve(__dirname, '../upload')));

app.use(sendIndex);

app.get('/robots.txt', robots);

// catch 404 and forward to error handler
app.get('*', notFound);
app.get(notFound);

// if error is not an instanceOf APIError, convert it.
app.use(errorConverter);

// error handler, send stacktrace only during development
app.use(errorHandler);

export default server;
