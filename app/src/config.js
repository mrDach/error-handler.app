require('dotenv').config();

const availableEnvs = ['production', 'test', 'development', 'stage',];
const env = availableEnvs.includes(process.env.NODE_ENV) ? process.env.NODE_ENV : 'production';

export default {
    jwtSecret: 'test',
    passportOptions: {
        session: false
    },
    serverUrl: process.env.SERVER_URL || 'http://localhost:3001',
    env: {
        production: env === 'production',
        test: env === 'test',
        development: env === 'development',
        stage: env === 'stage',
        toString() {
            return env;
        },
    },
    mongooseIntl: {
        languages: [
            'en', // English
            'ar', // Arabic
            'zh', // Chinese
            'hi', // Hindi
            'ja', // Japanese
            'ko', // Korean
            'fr', // French
            'de', // German
            'it', // Italian
            'pl', // Polish
            'pt', // Portuguese
            'es', // Spanish,
            'ru', // Russian
        ],
        defaultLanguage: 'en'
    },
};
