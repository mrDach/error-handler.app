import multer, {diskStorage} from 'multer';
import uuid from 'uuid';
import path from 'path';

import {ValidationError} from "../helpers/APIError";
import {invalidFileType} from "../constants/errorMessages";
import {checkUploadValidationErrors} from "./checkValidationErrors";

const pathToUploadDir = path.resolve(__dirname, `../../upload`);
const publicPathToUploadDir = '/upload';

export function getFilename(req, file, cb) {
    const filename = uuid.v4() + getFileExtension(file.originalname);// add file extension
    if (!cb) {
        return filename;
    } else {
        cb(null, filename);
    }
}

export function getFileExtension(originalName) {
    return path.extname(originalName).toLowerCase();
}

function filterFileByExtension(file, regExpFileTypes) {
    const mimetype = regExpFileTypes.test(file.mimetype);
    const extname = regExpFileTypes.test(getFileExtension(file.originalname));
    return mimetype && extname;
}

/**
 * @param {string} pathToDir
 * @param {RegExp} fileTypes
 * @param {Number} maxFileSizeInMB
 * @returns {*}
 */
const upload = (pathToDir, fileTypes, maxFileSizeInMB) => {
    const storage = diskStorage({
        destination: pathToDir,
        filename: getFilename,
    });

    return multer({
        storage,
        limits: {fileSize: maxFileSizeInMB * 1024 * 1024},
        fileFilter(req, file, cb) {
            if (filterFileByExtension(file, fileTypes)) {
                return cb(null, true);
            }
            return cb(new ValidationError([
                {
                    location: "files",
                    param: file.fieldname,
                    msg: invalidFileType,
                }
            ]));
        },
    });
};

export const pathToUploadDocumentDir = path.resolve(pathToUploadDir, 'documents');
export const publicPathToUploadDocumentDir = path.resolve(publicPathToUploadDir, 'documents');
export const uploadDocument = [upload(pathToUploadDocumentDir, /png|jpeg|jpg|pdf/, 25).single('file'), checkUploadValidationErrors];

export const pathToUploadAvatarDir = path.resolve(pathToUploadDir, 'avatars');
export const publicPathToUploadAvatarDir = path.resolve(publicPathToUploadDir, 'avatars');
export const uploadAvatar = [upload(pathToUploadAvatarDir, /png|jpeg|jpg/, 3.5).single('avatar'), checkUploadValidationErrors];

export const pathToUploadExchangerLogoDir = path.resolve(pathToUploadDir, 'exchangers');
export const publicPathToUploadExchangerLogoDir = path.resolve(publicPathToUploadDir, 'exchangers');
export const uploadExchangerLogo = [upload(pathToUploadExchangerLogoDir, /png|jpeg|jpg|svg/, 3.5).single('file'), checkUploadValidationErrors];

export const pathToUploadBranchesDir = path.resolve(pathToUploadDir, 'branches');
export const publicPathToUploadBranchesDir = path.resolve(publicPathToUploadDir, 'branches');
export const uploadBranches = [upload(pathToUploadBranchesDir, /png|jpeg|jpg/, 3.5).single('file'), checkUploadValidationErrors];

export const pathToUploadRoadmapDir = path.resolve(pathToUploadDir, 'roadmap');
export const publicPathToUploadRoadmapDir = path.resolve(publicPathToUploadDir, 'roadmap');
export const uploadRoadmap = [upload(pathToUploadRoadmapDir, /png|jpeg|jpg|svg/, 3.5).single('file'), checkUploadValidationErrors];

export const pathToUploadDecentralizedBankDir = path.resolve(pathToUploadDir, 'decentralizedBank');
export const publicPathToUploadDecentralizedBankDir = path.resolve(publicPathToUploadDir, 'decentralizedBank');
export const uploadDecentralizedBank = [upload(pathToUploadDecentralizedBankDir, /png|jpeg|jpg|svg/, 3.5).single('file'), checkUploadValidationErrors];

export const pathToUploadPartnerDir = path.resolve(pathToUploadDir, 'partners');
export const publicPathToUploadPartnerDir = path.resolve(publicPathToUploadDir, 'partners');
export const uploadPartnerFiles = [upload(pathToUploadPartnerDir, /png|jpeg|jpg|svg/, 3.5).fields([
    {name: 'logo', maxCount: 1},
    {name: 'photo', maxCount: 1},
]), checkUploadValidationErrors];
