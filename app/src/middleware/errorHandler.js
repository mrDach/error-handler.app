import httpStatus from "http-status";

import config from "../config";

export const errorHandler = (err, req, res, next) => {
    console.log(err);

    const objectOfResponse = {
        name: err.name,
        success: false,
        message: err.isPublic ? err.message : httpStatus[err.status],
    };

    if (config.env.development) {
        objectOfResponse.stack = err.stack;
    }

    res.status(err.status).json(objectOfResponse);
    return next();
};
