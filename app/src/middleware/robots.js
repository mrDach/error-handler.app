import httpStatus from "http-status";
import path from "path";

import config from "../config";

export const robots = (req, res, next) => {
    res.status(httpStatus.OK);
    let file = 'robots.prod.txt';
    if (config.env.development || config.env.stage) {
        file = 'robots.dev.txt';
    }
    const filePath = path.resolve(__dirname, '../../../clientApp/client/build', file);
    res.sendFile(filePath);
};
