import {MulterError} from "multer";

import {ValidationError} from "../helpers/APIError";

export const checkValidationErrors = (req, res, next) => {
    const errors = req.validationErrors();
    if (errors) {
        throw new ValidationError(errors);
    }
    return next();
};

export const checkUploadValidationErrors = (err, req, res, next) => {
    if (err instanceof MulterError) {
        // A Multer error occurred when uploading.
        req.pushValidationErrors([{
            location: "files",
            param: err.field,
            msg: err.code,
        }]);
        next();
    } else if (err instanceof ValidationError) {
        req.pushValidationErrors(err.message);
        next();
    } else {
        // An unknown error occurred when uploading.
        next(err);
    }
};

export const addPushValidationErrors = () => {
    return (req, res, next) => {
        req.pushValidationErrors = (errors) => {
            req._validationErrors = (req._validationErrors || []).concat(errors);
        };
        next();
    }
};
