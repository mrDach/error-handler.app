import UserSchema from '../models/user';

import config from '../config';
import httpStatus from 'http-status';
import {AuthError} from '../helpers/APIError';
import jwt from "jsonwebtoken";
import mongoose from "mongoose";

const UserModel = mongoose.model('User');

export const validateAdmin = async (req, res, next) => {
    try {
        let bearerHeader = req.headers['authorization'];
        let token;
        req.user = null;
        if (!bearerHeader){
            throw new AuthError('token not found', httpStatus.BAD_REQUEST);
        }
        let bearer = bearerHeader.split(" ");
        token = bearer[1];
        const decoded = await jwt.verify(token, config.jwtSecret);

        const user = await UserModel.findOne({_id: decoded.id});
        if (!user) {
            throw new AuthError('Incorrect login or password');
        }
        req.user = user;
        return next();
    } catch (err) {
        console.log(err);
        next(new AuthError('token not found'));
    }
};

export default { validateAdmin };
