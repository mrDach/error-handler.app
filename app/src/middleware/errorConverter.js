import fs from 'fs';
import {promisify} from "util";

const promisifyUnlink = promisify(fs.unlink);

import {APIError, AuthError, NotFoundError, ValidationError} from "../helpers/APIError";

export const errorConverter = async (err, req, res, next) => {

    if (req.file) {
        await promisifyUnlink(req.file.path);
    }

    if (err instanceof ValidationError) {
        return next(err);
    } else if (err instanceof AuthError) {
        return next(err);
    } else if (err instanceof NotFoundError) {
        return next(err);
    } else {
        const apiError = new APIError(err.message, err.status, err.isPublic);
        return next(apiError);
    }
};
