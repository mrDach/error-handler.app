import httpStatus from "http-status";
import path from "path";

export const notFound = (req, res, next) => {
    res.status(httpStatus.NOT_FOUND);
    const filePath = path.resolve(__dirname, '../../../clientApp/client/build', 'index.html');
    res.sendFile(filePath);
};
