import expressValidator from "express-validator";

import {
    haveFile,
    name,
    searchText,
    isMomentValid,
    phone,
    textNormal,
    skypeUsername,
    telegramUsername,
    email,
} from "../helpers/validation";
import {addPushValidationErrors} from "./checkValidationErrors";

export const addExpressValidator = [
    (req, res, next) => expressValidator({
        customValidators: {
            isValidName: name,
            isValidSearch: searchText,
            isNormalText: textNormal,
            isMomentValid: isMomentValid,
            isValidPhone: phone,
            isValidSkypeUsername: skypeUsername,
            isValidTelegramUsername: telegramUsername,
            isHaveFile: haveFile(req),
            isValidEmail: email,
        }
    })(req, res, next),
    addPushValidationErrors(),
];
