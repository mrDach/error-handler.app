export const noop = (res) => {
    if (res instanceof Error) {
        throw res;
    }
    return res;
};
