import moment from 'moment';
import * as libphonenumber from 'libphonenumber-js';

export const name = (value) => !(value && /[±~!?.,:;*&`"№_@#$%^<>+={}()[\]|\\0-9]+/.test(value));

export const searchText = (value) => !(value && /[±~!?@#$%^<>+={}:;[\]|\\]+/.test(value));

export const textNormal = (value) => !(value && /[±~!?@#$%^<>+={}:;[\]|\\]+/.test(value));

export const skypeUsername = (value) => !(value && !/^[a-zA-Z0-9_.\-:]+$/.test(value));

export const telegramUsername = (value) => !(value && !/^@?([a-zA-Z0-9]{5,32})+$/.test(value));

export const haveFile = (req) => function (test, fieldName) {
    return req.file && req.file.fieldname === fieldName || req.files && (fieldName in req.files);
};

export const isMomentValid = (value, format) => moment(value, format).isValid();

export const phone = (value) => value !== undefined && value.length && libphonenumber.isValidNumber(value);

export const email = (value) => !(value && !/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(value));
