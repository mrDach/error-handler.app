import nodemailer from 'nodemailer';

export const sendMail = async (mailOptions) => {
    const transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'apollo@firstbridge.io',
            pass: '8MT8N*jN'
        }
    });

    const defaultMailOptions = {
        from: '"Apollo Support" <pr@apollocurrency.com>',
        to: 'Pr@apollocurrency.com',
        subject: 'Apollo',
        ...mailOptions,
    };

    await transporter.sendMail(defaultMailOptions);
};
