import fs from 'fs';
import path from 'path';
import {promisify} from "util";

const promisifyUnlink = promisify(fs.unlink);

/**
 * @param newData
 * @param prevData
 * @param pathToUpload
 * @param propertiesForCleanup
 */
export const cleanupData = async (newData, prevData, pathToUpload, propertiesForCleanup) => {
    const result = {};
    const unset = {};
    const dataForSend = {...newData};
    let unsetIsDirty = false;
    propertiesForCleanup && propertiesForCleanup.forEach(async (item) => {
        if (newData.hasOwnProperty(item) && (newData[item] === null || newData[item] === 'null')) { // new entity hasn't file
            unset[item] = 1;
            unsetIsDirty = true;
            delete dataForSend[item];
        }
        if (prevData.hasOwnProperty(item) && prevData[item].filename) { // old entity has file
            await promisifyUnlink(path.resolve(pathToUpload, prevData[item].filename));
        }
    });
    result['$set'] = dataForSend;
    if (unsetIsDirty) {
        result['$unset'] = unset;
    }
    return result;
};
