/**
 * Deep merge two objects.
 * @param target
 * @param source
 */
export function mergeDeep(target, source) {
    // Iterate through `source` properties and if an `Object` set property to merge of `target` and `source` properties
    for (let key of Object.keys(source)) {
        if (!target.hasOwnProperty(key)) {
            target[key] = {};
        }
        if (source[key] instanceof Object) {
            target[key] = mergeDeep(target[key], source[key]);
        } else {
            target[key] = source[key];
        }
    }

    return target;
}
