import rimraf from "rimraf";
import fs from "fs";
import {promisify} from "util";

const promisifyMkdir = promisify(fs.mkdir);
const promisifyExists = promisify(fs.exists);

export async function cleanupDir(pathToDir) {
    if (await promisifyExists(pathToDir)) {
        await new Promise((resolve, reject) => {
            rimraf(pathToDir, (err, data) => {
                if (err) reject(err);
                resolve(data);
            })
        });
        console.log(`${pathToDir} cleanup`);
    }
    await promisifyMkdir(pathToDir);
}
