import mime from "mime-types";
import path from "path";
import fs from "fs";
import {promisify} from "util";

import {getFilename} from "../middleware/upload";

const promisifyCopyFile = promisify(fs.copyFile);
const promisifyStat = promisify(fs.stat);

export async function copyFile(fileName, pathFromUpload, pathToUpload) {
    const unicFileName = getFilename({}, {originalname: fileName});
    const from = path.resolve(__dirname, pathFromUpload, fileName);
    const to = path.resolve(pathToUpload, unicFileName);
    await promisifyCopyFile(from, to);
    const stats = await promisifyStat(from);
    const mimeType = mime.lookup(from);

    return {
        unicFileName,
        stats,
        mimeType,
    };
}
