import {
    notEmpty,
    invalidLength,
    invalidMongoId,
    invalidName,
    invalidBoolean,
    invalidSearch,
    invalidEmail,
    invalidPhone,
    invalidFiles,
    invalidCurrentLanguage,
    invalidAfter,
    invalidBefore,
    invalidTime,
    invalidPosition,
} from "../constants/errorMessages";
import config from "../config";

export const getPublicBranchValidator = (req, res, next) => {
    req.checkQuery('address')
        .optional({checkFalsy: true})
        .isValidSearch().withMessage(invalidSearch)
        .isLength({min: 2, max: 300}).withMessage(invalidLength);

    return next();
};

export const getBranchValidator = (req, res, next) => {
    req.checkParams('id')
        .notEmpty().withMessage(notEmpty)
        .isMongoId().withMessage(invalidMongoId);

    return next();
};

export const saveBranchValidator = (req, res, next) => {
    const {
        _id,
        currentLanguage,
        fullName,
        email,
        title,
        address,
        position,
        additionalInfo,
        workTime,
        tel,
        isPublic,
    } = req.body;
    req.body = {
        _id,
        currentLanguage,
        fullName,
        email,
        title,
        address,
        position,
        additionalInfo,
        workTime,
        tel,
        isPublic,
    };

    req.checkBody('currentLanguage')
        .notEmpty().withMessage(notEmpty)
        .isIn(config.mongooseIntl.languages).withMessage(invalidCurrentLanguage);

    req.checkBody('_id')
        .optional({checkFalsy: true})
        .notEmpty().withMessage(notEmpty)
        .isMongoId().withMessage(invalidMongoId);

    if (!req.body._id) {
        req.checkBody('currentLanguage')
            .equals(config.mongooseIntl.defaultLanguage).withMessage(invalidCurrentLanguage);
    }

    req.checkBody('fullName')
        .notEmpty().withMessage(notEmpty)
        .isLength({min: 2, max: 300}).withMessage(invalidLength)
        .isValidName().withMessage(invalidName);

    req.checkBody('email')
        .notEmpty().withMessage(notEmpty)
        .isLength({max: 255}).withMessage(invalidLength)
        .isValidEmail().withMessage(invalidEmail);

    if (req.body.currentLanguage) {
        req.checkBody(`title.${req.body.currentLanguage}`)
            .notEmpty().withMessage(notEmpty)
            .isLength({max: 100}).withMessage(invalidLength);
    }

    req.checkBody('file')
        .isHaveFile('file').withMessage(invalidFiles);

    req.checkBody('address')
        .notEmpty().withMessage(notEmpty);

    req.checkBody('position.lat')
        .notEmpty().withMessage(notEmpty)
        .isFloat({min: -90, max: 90}).withMessage(invalidPosition);

    req.checkBody('position.lng')
        .notEmpty().withMessage(notEmpty)
        .isFloat({min: -180, max: 180}).withMessage(invalidPosition);

    req.checkBody('additionalInfo')
        .isLength({max: 300}).withMessage(invalidLength);

    req.checkBody('workTime.open')
        .notEmpty().withMessage(notEmpty)
        .isMomentValid('X').isInt({min: 0}).withMessage(invalidTime);

    req.checkBody('workTime.close')
        .notEmpty().withMessage(notEmpty)
        .isMomentValid('X').isInt({max: 3600 * 24}).withMessage(invalidTime);

    if (req.body.workTime && req.body.workTime.open && req.body.workTime.close) {
        req.checkBody('workTime.open')
            .isInt({max: req.body.workTime.close - 10 * 60}).withMessage(invalidAfter);

        req.checkBody('workTime.close')
            .isInt({min: req.body.workTime.open + 10 * 60}).withMessage(invalidBefore);
    }

    req.checkBody('tel')
        .notEmpty().withMessage(notEmpty)
        .isValidPhone().withMessage(invalidPhone);

    req.checkBody('isPublic')
        .optional({checkFalsy: true})
        .toBoolean(true)
        .isBoolean().withMessage(invalidBoolean);

    return next();
};

export const saveUnpublicBranchValidator = (req, res, next) => {
    const {
        currentLanguage,
        fullName,
        email,
        title,
        address,
        position,
        workTime,
        tel,
    } = req.body;
    req.body = {
        currentLanguage,
        fullName,
        email,
        title,
        address,
        position,
        workTime,
        tel,
    };

    req.checkBody('currentLanguage')
        .notEmpty().withMessage(notEmpty)
        .isIn(config.mongooseIntl.languages).withMessage(invalidCurrentLanguage);

    req.checkBody('_id')
        .optional({checkFalsy: true})
        .notEmpty().withMessage(notEmpty)
        .isMongoId().withMessage(invalidMongoId);

    if (!req.body._id) {
        req.checkBody('currentLanguage')
            .equals(config.mongooseIntl.defaultLanguage).withMessage(invalidCurrentLanguage);
    }

    req.checkBody('fullName')
        .notEmpty().withMessage(notEmpty)
        .isLength({min: 2, max: 300}).withMessage(invalidLength)
        .isValidName().withMessage(invalidName);

    req.checkBody('email')
        .notEmpty().withMessage(notEmpty)
        .isLength({max: 255}).withMessage(invalidLength)
        .isValidEmail().withMessage(invalidEmail);

    if (req.body.currentLanguage) {
        req.checkBody(`title.${req.body.currentLanguage}`)
            .notEmpty().withMessage(notEmpty)
            .isLength({max: 100}).withMessage(invalidLength);
    }

    req.checkBody('file')
        .isHaveFile('file').withMessage(invalidFiles);

    req.checkBody('address')
        .notEmpty().withMessage(notEmpty);

    req.checkBody('position.lat')
        .notEmpty().withMessage(notEmpty)
        .isFloat({min: -90, max: 90}).withMessage(invalidPosition);

    req.checkBody('position.lng')
        .notEmpty().withMessage(notEmpty)
        .isFloat({min: -180, max: 180}).withMessage(invalidPosition);

    req.checkBody('additionalInfo')
        .isLength({max: 300}).withMessage(invalidLength);

    req.checkBody('workTime.open')
        .notEmpty().withMessage(notEmpty)
        .isMomentValid('X').isInt({min: 0}).withMessage(invalidTime);

    req.checkBody('workTime.close')
        .notEmpty().withMessage(notEmpty)
        .isMomentValid('X').isInt({max: 3600 * 24}).withMessage(invalidTime);

    if (req.body.workTime && req.body.workTime.open && req.body.workTime.close) {
        req.checkBody('workTime.open')
            .isInt({max: req.body.workTime.close - 10 * 60}).withMessage(invalidAfter);

        req.checkBody('workTime.close')
            .isInt({min: req.body.workTime.open + 10 * 60}).withMessage(invalidBefore);
    }

    req.checkBody('tel')
        .notEmpty().withMessage(notEmpty)
        .isValidPhone().withMessage(invalidPhone);

    return next();
};

export const togglePublicBranchValidator = (req, res, next) => {
    req.checkBody('_id')
        .notEmpty().withMessage(notEmpty)
        .isMongoId().withMessage(invalidMongoId);

    req.checkBody('isPublic')
        .notEmpty().withMessage(notEmpty)
        .toBoolean(true)
        .isBoolean().withMessage(invalidBoolean);

    return next();
};

export const deleteBranchValidator = (req, res, next) => {
    req.checkBody('_id')
        .notEmpty().withMessage(notEmpty)
        .isMongoId().withMessage(invalidMongoId);

    return next();
};
