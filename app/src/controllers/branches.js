import BranchSchema from '../models/branch';
import ReviewSchema from '../models/review';

import fs from 'fs';
import path from 'path';
import mongoose from 'mongoose';
import {promisify} from "util";

import {cleanupData} from "../helpers/cleanupData";
import {pathToUploadBranchesDir} from "../middleware/upload";
import config from "../config";

const promisifyUnlink = promisify(fs.unlink);

const defaultLanguage = config.mongooseIntl.defaultLanguage;

const BranchModel = mongoose.model('Branch');
const ReviewModel = mongoose.model('Review');

export const getBranches = async (req, res, next) => {
    try {
        const branches = await BranchModel.find().select().sort({priority: 1});
        res.json({success: true, branches});
    } catch (err) {
        next(err);
    }
};

export const getBranch = async (req, res, next) => {
    try {
        const branch = await BranchModel.findById(req.params.id);
        const ratingArr = await ReviewModel.aggregate([
            {
                $match: {
                    isPublic: true,
                    branchId: mongoose.Types.ObjectId(req.params.id),
                }
            },
            {
                $group: {_id: req.params.id, average: {$avg: '$rating'}}
            }]);
        const {average} = ratingArr[0] || {average: 0};
        res.json({success: true, branch: {...branch.toObject(), rating: average}});
    } catch (err) {
        next(err);
    }
};

export const getPublicBranch = async (req, res, next) => {
    try {
        const {address} = req.query;
        const query = {isPublic: true};
        if (address) {
            const andQuery = [];
            const terms = `${address}`.split(/,|\s/g).filter(el => el.length > 0);
            terms.forEach((item) => {
                const regexpItem = new RegExp(item, 'i');
                andQuery.push({
                    $or: [
                        {address: regexpItem},
                        {additionalInfo: regexpItem},
                        {[`title.${defaultLanguage}`]: regexpItem},
                    ],
                });
            });
            query.$and = andQuery;
        }
        const branches = await BranchModel.find(query);
        res.json({success: true, branches});
    } catch (err) {
        next(err);
    }
};

export const saveBranch = async (req, res, next) => {
    try {
        const branch = {
            ...req.body,
        };

        if (req.file) {
            branch.file = {
                name: req.file.originalname,
                filename: req.file.filename,
                type: req.file.mimetype,
                size: req.file.size,
            };
        }

        if (!req.user) {
            branch.isPublic = false;
        }

        if (branch._id) {
            const prevBranch = await BranchModel.findOne({_id: branch._id});
            const data = await cleanupData(branch, prevBranch.toObject(), pathToUploadBranchesDir, ['file']);
            const updatedBranch = await BranchModel.findOneAndUpdate({_id: branch._id}, data, {new: true});
            return res.json({success: true, branch: updatedBranch});
        } else {
            const newBranch = new BranchModel(branch);
            // if (!req.user) {
            //     await sendJoinNewBranch(branch);
            // }
            await newBranch.save();
            return res.json({success: true, branch: newBranch});
        }
    } catch (err) {
        next(err);
    }
};

export const togglePublicBranch = async (req, res, next) => {
    try {
        const {_id, isPublic} = req.body;
        const branch = await BranchModel.findOneAndUpdate({_id}, {isPublic}, {new: true});
        res.json({success: true, branch});
    } catch (err) {
        return next(err);
    }
};

export const deleteBranch = async (req, res, next) => {
    try {
        const branch = await BranchModel.findById(req.body._id);
        if (branch) {
            await promisifyUnlink(path.resolve(pathToUploadBranchesDir, branch.file.filename));
            await branch.remove();
        }

        res.json({success: true});
    } catch (err) {
        return next(err);
    }
};
