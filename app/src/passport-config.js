import passportJWT from 'passport-jwt';
import cfg from './config';
import UserSchema from './models/user';
const ExtractJwt = passportJWT.ExtractJwt;
const jwtStrategy = passportJWT.Strategy;

function passportConfiguration(passport) {
  const opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
  opts.secretOrKey = cfg.jwtSecret;
  passport.use(new jwtStrategy(opts, (jwtPayload, cb) => {
    UserSchema.findOneAsync({ _id: jwtPayload._doc._id })
      .then((user) => cb(null, user))
      .error((err) => cb(err, false));
  }));
}
export default passportConfiguration;
