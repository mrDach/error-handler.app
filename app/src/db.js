import Promise from 'bluebird';
import mongoose  from 'mongoose';

// promisify mongoose
Promise.promisifyAll(mongoose);
mongoose.Promise = Promise;

mongoose.connect('mongodb://db:27017/apl', { useNewUrlParser: true });
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
    console.log('Mongo db connected successfully');
});

export default db;
